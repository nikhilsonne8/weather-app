export const geoApiOptions = {
	method: 'GET',
	headers: {
		'X-RapidAPI-Key': '09f0c17143msha407fddf1df6d06p1ed67ajsn0544cbabaefb',
		'X-RapidAPI-Host': 'wft-geo-db.p.rapidapi.com'
	}
};

export const GEO_API_URL = "https://wft-geo-db.p.rapidapi.com/v1/geo";

export const WEATHER_API_URL = 'https://api.openweathermap.org/data/2.5';

export const  WEATHER_API_KEY = '421cb1ecced69e731131471c5eff505a';